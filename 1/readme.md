#Task 1

Create a **plain** JavaScript REST consumer for a small subset of the parliament API. Display the data you fetched in any way you like. Good styling is always a bonus.

[This is the endpoint you have to consume](http://ws-old.parlament.ch/). 

The entity you have to consume is called **Councillors**.

It has to be consumed from the link, not downloaded in a JSON file.
Enable sorting and filtering on the ID and Name properties. Bare in mind that you *must* not rely on sorting and filtering features provided by the API.

NOTE: The solution to this task has to be in a separate folder called `solution1` and has to be squashed into 1 commit. Please don't use any third-party libraries/packages. We want to be able to simply open the solution in a browser, **without** having to install dependencies/build bundles/run local servers/etc.
